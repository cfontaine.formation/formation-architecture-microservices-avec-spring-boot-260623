package fr.dawan.java8;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {

        // 1. une classe qui implemente l'interface
        Operation op1 = new Somme();
        System.out.println(Traitement(1, 3, op1));

        // 2. classe Annonyme
        System.out.println(Traitement(1, 3, new Operation() {

            @Override
            public int calcul(int a, int b) {
                return a + b;
            }
        }));

        // 3. Avec Java 8 Expression lamba 8
        System.out.println(Traitement(1, 3, (v1, v2) -> v1 + v2));
        System.out.println(Traitement(1, 3, (v1, v2) -> v1 * v2));

        // l'interface Runnable ne contient qu'une seule méthode, on peut utiliser
        // expression lambda pour créer un thread
        Thread t = new Thread(() -> System.out.println("Hello World"));
        t.start();

        List<Integer> lst = Arrays.asList(4, -1, 6, -7, 9);

        // l'interface Comparator ne contient qu'une seule méthode, on peut utiliser
        // expression lambda pour trier la liste
        Collections.sort(lst, (a, b) -> a > b ? -1 : 1);
        System.out.println(lst);

        // Référence de méthode static
        Message msg = () -> afficherMessage();
        msg.afficher();

        Message msg2 = App::afficherMessage;
        msg2.afficher();

        // Méthode forEach
        List<String> lstStr = Arrays.asList("John", "Jane", "Yves", "Michel", "Jimmy");

        lstStr.forEach((st) -> System.out.println(st));

        lstStr.forEach(System.out::println);

        // Stream
        // Méthode terminale foreach
        List<Integer> lstNum = Arrays.asList(4, -1, 6, -7, 9, 6, 3, -10, 2);
        lstNum.stream().map(n -> n * 2).filter(n -> n > 0 && n < 20).distinct().sorted()
                .forEach(i -> System.out.println(i));

        // Méthode terminale collect
        List<Integer> lrf = lstNum.stream().map(n -> n * 2).filter(n -> n > 0 && n < 20).distinct().sorted().limit(4)
                .collect(Collectors.toList());
        lrf.forEach(System.out::println);
        
        // Méthode terminale reduce
        int somme = lstNum.stream().map(n -> n * 2).filter(n -> n > 0 && n < 20).distinct().sorted().reduce(0,
                (a, b) -> a + b);
        System.out.println(somme);

        // Optionnal
        // Création d'un optional
        Optional<String> optVide = Optional.empty();
        Optional<String> hello = Optional.of("Hello");
        
        // Tester le contenu
        System.out.println(optVide.isEmpty());
        System.out.println(optVide.isPresent());

        // accéder au contenu -> get
        // si l'optionnal est vide -> exception lancer
        if (hello.isPresent()) {
            System.out.println(hello.get());
        }

        // Si l'Optional contient une valeur: Le consummer est exécuté
        hello.ifPresent(s -> System.out.println(s));
        optVide.ifPresent(s -> System.out.println(s));

        // Valeur par défaut si l'optional est vide
        System.out.println(optVide.orElse("valeur par defaut"));

        // Si l'Optional ne contient pas de valeur: Le supplier est exécuté
        System.out.println(optVide.orElseGet(() -> "c'est vide"));

        try {
         // Si l'Optional ne contient pas de valeur une exception est lancée
            String str = optVide.orElseThrow(() -> new Exception("c'est vide"));
            System.out.println(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int Traitement(int a, int b, Operation op) {
        return op.calcul(a, b);
    }

    public static void afficherMessage() {
        System.out.println("Message");
    }
}
