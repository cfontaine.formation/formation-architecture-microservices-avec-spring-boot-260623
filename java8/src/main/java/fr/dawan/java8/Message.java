package fr.dawan.java8;

@FunctionalInterface
public interface Message {
    void afficher();
}
