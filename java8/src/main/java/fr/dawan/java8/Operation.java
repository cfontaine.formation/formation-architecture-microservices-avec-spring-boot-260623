package fr.dawan.java8;
@FunctionalInterface
public interface Operation {
    int calcul(int a, int b);
}
