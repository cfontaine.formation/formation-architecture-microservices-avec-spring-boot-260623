package fr.dawan.java8;

public class Somme implements Operation {

    @Override
    public int calcul(int a, int b) {
        return a+b;
    }

}
