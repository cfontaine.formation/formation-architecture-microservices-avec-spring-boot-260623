package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.dawan.springcore.beans.DtoMapper;
import fr.dawan.springcore.components.ArticleRepository;

@Configuration // => classe de configuration
@ComponentScan("fr.dawan.springcore") // => scan des classes du package pour trouver les composants
                                      // @Component, @Repository, @Controller,@Service
public class AppConf {

    // Déclarer un bean une méthode annotée avec @Bean
    // Le type de retour est le type du bean, le nom du bean et le nom de la méthode
    @Bean
    public DtoMapper mapper1() {
        return new DtoMapper();
    }

    // L'attribut name de @Bean permet de définir le nom du bean (un ou plusieurs)
    // dans ce cas, le nom de la méthode n'est plus prix en compte
    @Bean(name = "repository2")
    // @Primary => s'il y a plusieurs beans du même type, avec un @Autowired, c'est
    // le bean annoté avec @Primary qui sera sélectionner
    public ArticleRepository getRepository() {
        return new ArticleRepository("source2");
    }

//    @Bean
//    public ArticleService service1(ArticleRepository repository1) {
//        return new ArticleService(repository1);
//    }

}
