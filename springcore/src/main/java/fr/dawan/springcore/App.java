package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.DtoMapper;
import fr.dawan.springcore.components.ArticleRepository;
import fr.dawan.springcore.components.ArticleService;

public class App {
    public static void main(String[] args) {
        // Création du conteneur d'ioc
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConf.class);

        // getBean -> permet de récupérer les instances des beans depuis le conteneur
        DtoMapper m1 = ctx.getBean("mapper1", DtoMapper.class);
        System.out.println(m1);

        ArticleRepository rep1 = ctx.getBean("repository1", ArticleRepository.class);
        System.out.println(rep1);

        ArticleService serv1 = ctx.getBean("service1", ArticleService.class);
        System.out.println(serv1);

        // Fermeture du context entraine la destruction de tous les beans
        ((AbstractApplicationContext) ctx).close();

    }
}
