package fr.dawan.springcore.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import lombok.Getter;
import lombok.ToString;

//@NoArgsConstructor
//@AllArgsConstructor
@Getter
//@Setter
@ToString
@Service(value = "service1")
public class ArticleService {

    // @Autowired
    // @Qualifier("repository2")
    private ArticleRepository repository;

    public ArticleService() {
        System.out.println("Constructeur par défaut ");
    }

    // @Autowired(required = false)
    public ArticleService(/* @Qualifier("repository1") */ArticleRepository repository) {
        System.out.println("Constructeur Repository ");
        this.repository = repository;
    }

    @Autowired
    public void setRepository(@Qualifier("repository1") ArticleRepository repository) {
        System.out.println("Setter Repository");
        this.repository = repository;
    }

}
