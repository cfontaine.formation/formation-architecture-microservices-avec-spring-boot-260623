package fr.dawan.springboot.security;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    @Autowired
    private UserDetailsService service;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        // On recupère le Bearer token dans l'entête Authorization de la requête
        String requestTokenHeader = request.getHeader("Authorization");
        
        // On test s'il existe et s'il commence par Bearer
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            // On supprime "Bearer ", pour récupérer le token jwt
            String jwtToken = requestTokenHeader.substring(7);
            // On récupère le username du claim sub (subject) du payload du token
            String username = jwtTokenUtil.getUsernameForToken(jwtToken);
            // On vérifie qu'il existe et que le usernamePasswordAuthenticationToken n'est pas présent dans le SecurityContextHolder
            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                // On récupère le Userdetails depuis la bdd en fonction de username du token 
                UserDetails userDetails = service.loadUserByUsername(username);
                // Si le token est valide, on configure Spring Security pour définir manuellement l'authentification
                if (jwtTokenUtil.valideJwtToken(jwtToken, userDetails)) {
                    UsernamePasswordAuthenticationToken userPasswordAuthtoken = new UsernamePasswordAuthenticationToken(userDetails,
                            null, userDetails.getAuthorities());
                    userPasswordAuthtoken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    // Après avoir définie l'authentification dans le security context, on spécifie que l'utilisateur courant est authentifié.
                    SecurityContextHolder.getContext().setAuthentication(userPasswordAuthtoken);
                }
            }
        }
        // on passe au prochain filtre de la chaîne, le dernier élément de la chaîne est la dispacherservlet
        filterChain.doFilter(request, response);
    }

}
