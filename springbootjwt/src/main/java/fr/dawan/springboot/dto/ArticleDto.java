package fr.dawan.springboot.dto;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ArticleDto {
    
    private long id;
    
    @Min(value=0,message = "Le prix n'est pas < 0")
    private double prix;
    
    @NotNull
    @Size(max=255)
    private String description;
    
    @NotNull
    @PastOrPresent
    @JsonProperty("date-production")
    @JsonFormat(shape = Shape.STRING,pattern = "dd-MM-yy")
    private LocalDate dateProduction;
    
    private byte[] image;
    
    private MarqueDto marque;
    
    private List<FournisseurDto> fournisseurs;
    
  //  private long marqueId;
    
    //private List<Long> founisseursId;

}
