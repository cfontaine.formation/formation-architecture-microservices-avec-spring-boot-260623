package fr.dawan.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.dto.JwtRequestDto;
import fr.dawan.springboot.dto.JwtResponseDto;
import fr.dawan.springboot.security.JwtTokenUtil;

@RestController
public class JwtAuthorisationController {
    
    @Autowired
    private AuthenticationManager authencationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    @Autowired
    private UserDetailsService userDetailService;

    @PostMapping(value="/authenticate")
    public ResponseEntity<?> createToken(@RequestBody JwtRequestDto dtorequest) throws Exception{
        try {
            authencationManager.authenticate(new UsernamePasswordAuthenticationToken(dtorequest.getUsername(), dtorequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS",e);
        }
        UserDetails ud= userDetailService.loadUserByUsername(dtorequest.getUsername());
        String jwtToken=jwtTokenUtil.generateJwtToken(ud);
        return ResponseEntity.ok(new JwtResponseDto(jwtToken));
    }
}
