package fr.dawan.springboot.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.springboot.dto.ArticleDto;
import fr.dawan.springboot.services.ArticleService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/articles")
public class ArticleController extends GenericController<ArticleDto, Long> {

    @Autowired
    private ObjectMapper objectMapper;
    
    public ArticleController(ArticleService service) {
        super(service);
    }

    @PostMapping(value="/upload/{id}", consumes="multipart/form-data",produces="application/json" )
    public ResponseEntity<ArticleDto> uploadImage(@PathVariable long id,@RequestParam("image") MultipartFile file) throws IOException{
        ArticleDto a=((ArticleService) service).getById(id);
        System.out.println(file.getOriginalFilename());
        a.setImage(file.getBytes());
        ((ArticleService) service).saveOrUpdate(a);
        return ResponseEntity.ok(a);
    }
    
    @PostMapping(consumes="multipart/form-data",produces="application/json")
    public ResponseEntity<ArticleDto> uploadImage(@RequestParam("article") String articleStr,@RequestParam("image") MultipartFile file) throws IOException{
        ArticleDto articleDto=objectMapper.readValue(articleStr, ArticleDto.class);
        articleDto.setImage(file.getBytes());
        return ResponseEntity.ok( ((ArticleService) service).saveOrUpdate(articleDto));
    }

    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ArticleDto create( @Valid @RequestBody ArticleDto dto) {
        return ((ArticleService) service).saveOrUpdate(dto);
    }
    
    
}
