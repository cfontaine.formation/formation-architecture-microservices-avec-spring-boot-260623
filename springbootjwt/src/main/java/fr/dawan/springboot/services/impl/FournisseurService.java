package fr.dawan.springboot.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dto.FournisseurDto;
import fr.dawan.springboot.entities.Fournisseur;
import fr.dawan.springboot.repositories.FournisseurRepository;
@Service
@Transactional
public class FournisseurService extends GenericServiceImpl<FournisseurDto, Fournisseur, Long>
        implements fr.dawan.springboot.services.FournisseurService {

    public FournisseurService(FournisseurRepository repository, ModelMapper mapper){
        super(repository, mapper, Fournisseur.class, FournisseurDto.class);
    }

}
