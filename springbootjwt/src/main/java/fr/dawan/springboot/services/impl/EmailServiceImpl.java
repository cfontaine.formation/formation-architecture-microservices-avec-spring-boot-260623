package fr.dawan.springboot.services.impl;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import fr.dawan.springboot.services.EmailService;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage.RecipientType;
@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    JavaMailSender sender;
    
    @Autowired
    private Configuration configuration;
    
    @Override
    public void sendSimpleMail(String content, String title, String to, String from) {
        SimpleMailMessage message=new SimpleMailMessage();
        message.setTo(to);
        message.setFrom(from);
        message.setSubject(title);
        message.setText(content);
        sender.send(message);
    }

    @Override
    public void sendHTMLMail(String content, String title, String to, String from) {
        sender.send(mimeMessage ->{
            mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(to));
            mimeMessage.setFrom(from);
            mimeMessage.setSubject(title,"utf-8");
            mimeMessage.setText(content,"utf-8","html");            
        });

    }

    @Override
    public void sendTemplateMail(String template, Map<String, Object> modelMap, String title, String to, String from) throws Exception{
        StringWriter sw=new StringWriter();
        configuration.getTemplate(template).process(modelMap, sw);
        sendHTMLMail(sw.toString(), title, to, from);
    }

    @Override
    public void sendMailAttachement(String content, String title, String to, String from, File file) {
        sender.send(mimeMessage ->{
            MimeMessageHelper message= new MimeMessageHelper(mimeMessage,true,"UTF-8");
            message.setTo(to);
            message.setFrom(from);
            message.setSubject(title);
            message.setText(content);   
            message.addAttachment(file.getName(), file);
        });

    }

}
