package fr.dawan.springboot.services;

import java.util.List;

import fr.dawan.springboot.dto.MarqueDto;

public interface MarqueGenService extends GenericService<MarqueDto, Long>{

    List<MarqueDto> getMarqueByNom(String nom);
}
