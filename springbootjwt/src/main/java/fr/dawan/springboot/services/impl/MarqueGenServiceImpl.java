package fr.dawan.springboot.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.entities.Marque;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.MarqueGenService;

@Service
@Transactional
public class MarqueGenServiceImpl extends GenericServiceImpl<MarqueDto, Marque, Long> implements MarqueGenService {


    public MarqueGenServiceImpl(MarqueRepository repository, ModelMapper mapper) {
        super(repository, mapper, Marque.class, MarqueDto.class);
    }

    @Override
    public List<MarqueDto> getMarqueByNom(String nom) {
        return ((MarqueRepository)repository).findByNomLike("%"+nom+"%").stream().map(m -> mapper.map(m, MarqueDto.class)).collect(Collectors.toList());
    }

}
