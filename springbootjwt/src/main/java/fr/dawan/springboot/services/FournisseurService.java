package fr.dawan.springboot.services;

import fr.dawan.springboot.dto.FournisseurDto;

public interface FournisseurService extends GenericService<FournisseurDto, Long> {

}
