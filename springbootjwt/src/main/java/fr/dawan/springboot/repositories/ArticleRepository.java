package fr.dawan.springboot.repositories;


import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import fr.dawan.springboot.entities.Article;
import fr.dawan.springboot.entities.Marque;


public interface ArticleRepository extends JpaRepository<Article, Long> {
    
    List<Article> findByPrix(double prix);
    
    List<Article> findByPrixLessThan(double prixMax);
    
    List<Article> findByPrixLessThanAndDateProductionBefore(double prixMax,LocalDate dateProdMax);

    List<Article> findByDescriptionLike(String model);
    
    List<Article> findByMarque(Marque marque);
    
    List<Article> findByMarqueNom(String nom);
    
    boolean existsByPrixGreaterThan(double prixMin);
    
    int countByMarqueNom(String nom);
    
    // void ou int
    int deleteByPrixLessThan(double prix);
    
    List<Article> findByMarqueNomIgnoreCase(String nom);
    
    List<Article> findByPrixLessThanOrderByPrixDescDescription(double prixMax);
    
    List<Article> findByMarqueNomOrderByPrixDesc(String nomMarque);
    
    List<Article> findByPrixGreaterThanOrderByMarqueNomDescPrix(double prixMin);
    
    List<Article> findByPrixIn(List<Double> prix);
    
    Article findFirstByOrderByPrixDesc();
    
    List<Article> findTop5ByOrderByPrix();
    
    Page<Article> findByPrixLessThan(double PrixMax,Pageable page);
    
    List<Article> findByPrixGreaterThan(double PrixMax,Pageable page);
    
    @Query("SELECT a FROM Article a JOIN a.fournisseurs f WHERE f.nom=:nom")
    List<Article> findByFournisseurNom(@Param("nom")String nomFournisseur);
    
    @Query(nativeQuery = true,value="SELECT * FROM articles WHERE prix<:prix")
    List<Article> findByPrixLessThanSQL(@Param("prix")double prixMax);
    
    // Procédure Stockée
    // Appel Explicite
    @Procedure("GET_COUNT_BY_PRIX2")
    int countInfMontant(double prix);
 // Appel implicite
    @Procedure
    int get_count_by_prix2(@Param("montant") double prix);
    
    @Query(nativeQuery = true,value="CALL GET_COUNT_BY_PRIX(:prix)")
    int countInfMontantSQL(@Param("prix")double prix);

    //@EntityGraph(value="article-fournisseur",type=EntityGraphType.LOAD)
    @EntityGraph(attributePaths = {"fournisseurs","marque"})
    List<Article> findByOrderByPrix();
}
