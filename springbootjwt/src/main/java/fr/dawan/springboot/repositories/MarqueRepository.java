package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.entities.Marque;

public interface MarqueRepository extends JpaRepository<Marque, Long> {
    @EntityGraph(value="marque-article-graph")
    // ou
    //@EntityGraph(attributePaths = {"articles"})
    List<Marque> findByNomLike(String model);
    
    int removeById(long id);
}
