package fr.dawan.springboot;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.springboot.controllers.MarqueController;
import fr.dawan.springboot.dto.MarqueDto;
// Test d'intégration
// - le plugin maven failsafe permet  d'éxécuter les tests d'intégrations
// - avec maven les méthodes qui finissent avec IT sont considérées comme des tests d'intégrations
// - pour éxécuter avec maven uniquement les tests d'intégrations
//   mvn failsafe::integration-test
@SpringBootTest
@ActiveProfiles("TEST")
@AutoConfigureMockMvc(addFilters = false)
@TestInstance(Lifecycle.PER_CLASS)
public class MarqueControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MarqueController marqueController;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void contextLoadsIT() {
        assertNotNull(marqueController);
    }

    @Test
    void getAllMarqueIT() throws Exception {
        String jsonRep = mockMvc.perform(get("/api/v1/marques")).andReturn().getResponse().getContentAsString();
        try {
            JSONAssert.assertEquals("[{\"id\":1,\"nom\":\"Marque A\"},{\"id\":2,\"nom\":\"Marque B\"}]", jsonRep,
                    false);
        } catch (JSONException e) {
            fail();
        }
    }

    @Test
    void getMarqueByIdIT() throws Exception {
        String jsonRep = mockMvc.perform(get("/api/v1/marques/2")).andReturn().getResponse().getContentAsString();
        try {
            JSONAssert.assertEquals("{\"id\":2,\"nom\":\"Marque B\"}", jsonRep, false);
        } catch (JSONException e) {
            fail();
        }
    }

    @Test
    void getMarqueByNameIT() throws Exception {
        String jsonRep = mockMvc.perform(get("/api/v1/marques/Marque")).andReturn().getResponse().getContentAsString();
        try {
            JSONAssert.assertEquals("[{\"id\":1,\"nom\":\"Marque A\"},{\"id\":2,\"nom\":\"Marque B\"}]", jsonRep,
                    false);
        } catch (JSONException e) {
            fail();
        }
    }

    @Test
    void getAllMarqueByNameIT() throws Exception {
        String jsonRep = mockMvc.perform(get("/api/v1/marques/Marque")).andReturn().getResponse().getContentAsString();
        try {
            JSONAssert.assertEquals("[{\"id\":1,\"nom\":\"Marque A\"},{\"id\":2,\"nom\":\"Marque B\"}]", jsonRep,
                    false);
        } catch (JSONException e) {
            fail();
        }
    }

    @Test
    void createMarqueIT() throws Exception {
        MarqueDto dto = new MarqueDto(0L, "Marque E");
        String jsonRep = mockMvc
                .perform(post("/api/v1/marques").contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto)))
                .andReturn().getResponse().getContentAsString();
        try {
            JSONAssert.assertEquals("{\"id\":3,\"nom\":\"Marque E\"}", jsonRep, false);
        } catch (JSONException e) {
            fail();
        }
    }

}
