package fr.dawan.springboot.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.services.MarqueService;

@WebMvcTest(MarqueController.class)
@ActiveProfiles("TEST")
@AutoConfigureMockMvc(addFilters = false)
public class MarqueControllerTest {

    @MockBean
    private MarqueService service;

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void getAllMarqueTest() throws Exception {
        List<MarqueDto> lstDto = new ArrayList<>();
        lstDto.add(new MarqueDto(1L, "marque A"));
        lstDto.add(new MarqueDto(2L, "marque B"));
        when(service.getAllMarque(Pageable.unpaged())).thenReturn(lstDto);
        String rep = mockMvc.perform(
                get("/api/v1/marques").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().getResponse().getContentAsString();
        try {
            JSONAssert.assertEquals("[{\"id\":1,\"nom\":\"marque A\"},{\"id\":2,\"nom\":\"marque B\"}]", rep, false);
        } catch (JSONException e) {
            fail();
        }
    }

    @Test
    public void getMarqueByIdTest() throws Exception {
        MarqueDto mDto = new MarqueDto(42L, "marque A");
        when(service.getMarqueById(42L)).thenReturn(mDto);
        String rep = mockMvc.perform(get("/api/v1/marques/42").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn().getResponse().getContentAsString();
        try {
            JSONAssert.assertEquals("{id:42,nom:\"marque A\"}", rep, JSONCompareMode.STRICT);
        } catch (JSONException e) {
            fail();
        }
    }

    @Test
    public void deleteMarqueByIdTest() throws Exception {
     when(service.deleteMarque(1L)).thenReturn(true);
     when(service.deleteMarque(2L)).thenReturn(false);
     int statusOk=mockMvc.perform(delete("/api/v1/marques/1")).andReturn().getResponse().getStatus();
     int status404=mockMvc.perform(delete("/api/v1/marques/2")).andReturn().getResponse().getStatus();
     assertEquals(200, statusOk);
     assertEquals(404,status404);
    }

    @Test
    public void createMarqueTest() throws UnsupportedEncodingException, Exception {
        MarqueDto dto = new MarqueDto(0L, "Marque A");
        when(service.saveOrUpdate(any(MarqueDto.class))).thenReturn(new MarqueDto(1L, "Marque A"));
        String jsonRep = mockMvc
                .perform(post("/api/v1/marques").contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(dto)))
                .andReturn().getResponse().getContentAsString();
        try {
            JSONAssert.assertEquals("{\"id\":1,\"nom\":\"Marque A\"}", jsonRep, false);
        } catch (JSONException e) {
            fail();
        }
    }
    
    public void updateMarqueTest() throws JsonProcessingException, UnsupportedEncodingException, Exception {
        MarqueDto dto = new MarqueDto(2L, "Marque BBB");
        when(service.getMarqueById(2L)).thenReturn(new MarqueDto(2L,"Marque B"));
        when(service.saveOrUpdate(any(MarqueDto.class)))
        .thenReturn(new MarqueDto(2L, "Marque BBB"));
        String jsonRep = mockMvc
                .perform(put("/api/v1/marques/2")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andReturn().getResponse().getContentAsString();
        try {
            JSONAssert.assertEquals("{\"id\":2,\"nom\":\"Marque BBB\"}", jsonRep, false);
        } catch (JSONException e) {
            fail();
        }      
    }
    

}