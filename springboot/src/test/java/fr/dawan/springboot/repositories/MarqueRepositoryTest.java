package fr.dawan.springboot.repositories;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.springboot.entities.Marque;

//Test unitaire
//- le plugin maven surefire permet  d'éxécuter les tests unitaire
//- avec maven les méthodes qui finissent avec Test sont considérées comme des tests unitaires
//- pour éxécuter avec maven les tests unitaire
//  mvn test

@DataJpaTest
@ActiveProfiles("TEST")
public class MarqueRepositoryTest {

    @Autowired 
    MarqueRepository repo;
    
    @Test
    public void repositotyTest() {
        assertNotNull(repo);
    }
    
    @Test
    public void findByNomLikeTest() {
        Marque m1=new Marque();
        m1.setId(1L);
        m1.setNom("Marque A");
        
        Marque m2=new Marque();
        m2.setId(2L);
        m2.setNom("Marque B");
        
        List<Marque> lst=repo.findByNomLike("%Marque%");
        assertEquals(2,lst.size());
        org.hamcrest.MatcherAssert.assertThat(m1,org.hamcrest.Matchers.samePropertyValuesAs(lst.get(0)));
        org.hamcrest.MatcherAssert.assertThat(m2,org.hamcrest.Matchers.samePropertyValuesAs(lst.get(1)));
    }
    
}
