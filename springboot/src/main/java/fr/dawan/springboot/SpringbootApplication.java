package fr.dawan.springboot;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootApplication.class, args);

//	    SpringApplication app=new SpringApplication(SpringbootApplication.class);
//	    app.setAddCommandLineProperties(false);
//	    app.run(args);
    }

    @Bean
    ModelMapper modelmapper() {
        return new ModelMapper();
    }
    
    @Bean
    RestTemplate restemplate() {
        return new RestTemplate();
    }

}
