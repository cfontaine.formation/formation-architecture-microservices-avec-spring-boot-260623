package fr.dawan.springboot.repositories;

import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Repository;

import fr.dawan.springboot.entities.Employe;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;

@Repository
public class EmployeCustomRepositoryImpl implements EmployeCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Employe> findBy(String prenom, String nom) {
        String requete = "SELECT e FROM Employe e";
        Map<String, String> params = new HashMap<>();
        if (prenom != null) {
            requete += " WHERE e.prenom=:prenom";
            params.put("prenom", prenom);
        }
        if (nom != null) {
            if (requete.contains("WHERE")) {
                requete += " AND e.nom=:nom";
            } else {
                requete += " WHERE e.nom=:nom";
            }
            params.put("nom", nom);
        }

        TypedQuery<Employe> tq = em.createQuery(requete, Employe.class);
        for (Entry<String, String> en : params.entrySet()) {
            tq.setParameter(en.getKey(), en.getValue());
        }
        return tq.getResultList();
    }

}
