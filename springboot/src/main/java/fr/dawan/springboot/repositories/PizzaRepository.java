package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springboot.entities.Pizza;

public interface PizzaRepository extends JpaRepository<Pizza, Long> {

   // @EntityGraph("pizza-ingredient")
    List<Pizza> findByNomLike(String modele);
}
