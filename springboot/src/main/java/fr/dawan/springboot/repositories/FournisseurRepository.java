package fr.dawan.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springboot.entities.Fournisseur;

public interface FournisseurRepository extends JpaRepository<Fournisseur, Long> {

    int removeById(long id);

}
