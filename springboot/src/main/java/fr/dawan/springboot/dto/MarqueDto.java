package fr.dawan.springboot.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

//@XmlRootElement
//@XmlAccessorType(XmlAccessType.FIELD)
public class MarqueDto {
    // @XmlAttribute
    @Include
    private long id;

    private String nom;

}
