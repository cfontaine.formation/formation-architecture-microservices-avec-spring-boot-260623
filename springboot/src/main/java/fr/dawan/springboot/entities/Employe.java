package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.AttributeOverrides;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.Version;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

@Entity
@Table(name = "employes")
//@SequenceGenerator(name="emp_sequence")
public class Employe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // @GeneratedValue(strategy = GenerationType.TABLE,generator ="employe_gen" )
    // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
    // "emp_sequence")
    @Include
    private long id;

    @Version
    private int version;

    @Column(length = 60)
    private String prenom;

    @Column(length = 60, nullable = false)
    private String nom;

    @Column(name = "date_naissance", nullable = false)
    private LocalDate dateNaissance;

    @Column(nullable = false, unique = true)
    private String email;

    @Transient
    private String nePasPersister;

    @Enumerated(EnumType.STRING)
    // @Column(length = 12)
    private Contrat contrat;

    @Lob
    @Column(length = 65000)
    private byte[] photo;

    @Embedded
    private Adresse adressePro;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "rue", column = @Column(name = "rue_perso")),
            @AttributeOverride(name = "ville", column = @Column(name = "ville_perso")),
            @AttributeOverride(name = "codePostal", column = @Column(name = "code_postal_perso")) })
    private Adresse adressePerso;

    @ElementCollection
    @CollectionTable(name = "telephones", joinColumns = @JoinColumn(name = "id_emp"))
    @Exclude
    private List<String> telephones = new ArrayList<>();

}
