package fr.dawan.springboot.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Embeddable
public class Adresse {
    
    @Column(nullable=false)
    private String rue;
    
    @Column(nullable=false, length = 50)
    private String ville;
    
    @Column(nullable=false, length = 15, name="code_postal")
    private String codePostal;
}
