package fr.dawan.springboot.entities;

import java.io.Serializable;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "pizzas_ingredients")
public class PizzaIngredient implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PizzaIngredientPK id;

    @ManyToOne
    @MapsId("pizzaId")
    private Pizza pizza;

    @ManyToOne
    @MapsId("ingredientId")
    private Ingredient ingredient;

    private int quantite;
}
