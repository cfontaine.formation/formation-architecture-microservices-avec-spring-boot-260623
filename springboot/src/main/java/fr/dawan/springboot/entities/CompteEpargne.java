package fr.dawan.springboot.entities;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter

@Entity
//@DiscriminatorValue("compte_epargne")
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;
    
    private double taux_interet;

}
