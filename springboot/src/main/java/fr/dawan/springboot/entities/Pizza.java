package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedAttributeNode;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "pizzas")
//@NamedEntityGraph(name = "pizza-ingredient",
//    attributeNodes = {  @NamedAttributeNode("ingredients"),
//                        @NamedAttributeNode("pizza"),
//                        @NamedAttributeNode("ingredient"),
//                        @NamedAttributeNode("pizzas") 
//                    })
public class Pizza implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private long id;

    @Version
    private int version;

    private String nom;

    @OneToMany(mappedBy = "pizza")
    private List<PizzaIngredient> ingredients = new ArrayList<>();
}
