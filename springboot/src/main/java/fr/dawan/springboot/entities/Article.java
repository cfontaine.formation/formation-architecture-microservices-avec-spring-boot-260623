package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedAttributeNode;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "articles")
@NamedEntityGraph(name="article-fournisseur",
attributeNodes = {
       // @NamedAttributeNode("marque"),
        @NamedAttributeNode("fournisseurs")
})
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(nullable=false, length = 150)
    private String description;
    
    @Column(nullable=false)
    private double prix;
    
    @Column(nullable=false,name="date_production")
    private LocalDate dateProduction;
    
    @Column(length=66000)
    @Exclude
    private byte[] image;
    
    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
   // @JoinColumn(name="id_marque")
    @Exclude
    private Marque marque;
    
    @ManyToMany
   // @JoinTable(name="fournisseur2article",
    //joinColumns=@JoinColumn(name="article_id"),
    //inverseJoinColumns = @JoinColumn(name="fournisseur_id"))
    @Exclude
    private List<Fournisseur> fournisseurs=new ArrayList<>();

    public Article(long id) {
        this.id = id;
    }
    
    
}
