package fr.dawan.springboot.runners;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.dto.ArticleDto;
import fr.dawan.springboot.dto.FournisseurDto;
import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.services.ArticleService;
import fr.dawan.springboot.services.FournisseurService;
import fr.dawan.springboot.services.MarqueService;
import lombok.extern.slf4j.Slf4j;

@Slf4j

//@Component
//@Order(2)
public class ServiceRunner implements ApplicationRunner {
    @Autowired
    private MarqueService marqueService;
    
    @Autowired
    private FournisseurService fournisseurService;
    
    @Autowired
    private ArticleService articleService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // Exemple logger
//        log.trace("Message TRACE");
//        log.debug("Message DEBUG");
//        log.info("Message INFO");
//        log.warn("Message WARN");
//        log.error("Message ERROR");

        log.info("ServiceRunner");
        
        marqueService.getAllMarque(Pageable.unpaged()).forEach(System.out::println);
        
        System.out.println(marqueService.getMarqueById(2L));
        
        System.out.println(marqueService.getMarqueByNom("Marque C"));
        
        System.out.println(marqueService.deleteMarque(1000L));
        
        MarqueDto md=new MarqueDto(0L,"Marque E");
        MarqueDto me=marqueService.saveOrUpdate(md);
        System.out.println(me);
        
        System.out.println(marqueService.deleteMarque(me.getId()));
        
        
        fournisseurService.getAll(Pageable.unpaged()).forEach(System.out::println);
        
        System.out.println(fournisseurService.getById(2L));
        
        FournisseurDto fd=new FournisseurDto(0L,"Fournisseur 4");
        FournisseurDto fe=fournisseurService.saveOrUpdate(fd);
        System.out.println(fe);
        
        fournisseurService.delete(fe.getId());
        
        System.out.println(articleService.getById(1L));
        

        
//        ArticleDto ad1=new ArticleDto(0L,"Repeteur WIFI",LocalDate.now(),2L);
//        System.out.println(articleService.saveOrUpdate(ad1));
        
    }

}
