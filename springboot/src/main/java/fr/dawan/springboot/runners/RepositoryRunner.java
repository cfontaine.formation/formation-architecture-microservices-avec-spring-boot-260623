package fr.dawan.springboot.runners;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.entities.Article;
import fr.dawan.springboot.entities.Marque;
import fr.dawan.springboot.repositories.ArticleRepository;
import fr.dawan.springboot.repositories.EmployeCustomRepository;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.repositories.PizzaRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j

//@Component
//@Order(1)
public class RepositoryRunner implements ApplicationRunner {

    @Autowired
    private ArticleRepository articleRepo;

    @Autowired
    private MarqueRepository marqueRepo;

    @Autowired
    private EmployeCustomRepository employeRepo;

    @Autowired
    private PizzaRepository pizzaRepo;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Repository runner");
        List<Article> lstArticle = articleRepo.findAll();
        lstArticle.forEach(System.out::println);
        System.out.println("-----------------");
        articleRepo.findByPrix(120.0).forEach(System.out::println);
        System.out.println("-----------------");
        articleRepo.findByPrixLessThan(50.0).forEach(System.out::println);
        System.out.println("-----------------");
        articleRepo.findByPrixLessThanAndDateProductionBefore(50.0, LocalDate.of(2018, 1, 1));
        System.out.println("-----------------");
        articleRepo.findByDescriptionLike("M__%").forEach(System.out::println);
        System.out.println("-----------------");
        articleRepo.findByMarqueNom("Marque A").forEach(System.out::println);

        System.out.println(articleRepo.existsByPrixGreaterThan(500.0));
        System.out.println(articleRepo.countByMarqueNom("Marque A"));
        System.out.println("-----------------");
        articleRepo.findByMarqueNomIgnoreCase("mArque a").forEach(System.out::println);
        System.out.println("-----------------");
        articleRepo.findByPrixLessThanOrderByPrixDescDescription(200.0).forEach(System.out::println);
        System.out.println("-----------------");
        articleRepo.findByMarqueNomOrderByPrixDesc("Marque A").forEach(System.out::println);
        System.out.println("-----------------");
        articleRepo.findByPrixGreaterThanOrderByMarqueNomDescPrix(50.0).forEach(System.out::println);
        System.out.println("-----------------");
        articleRepo.findByPrixIn(Arrays.asList(20.0, 650.0)).forEach(System.out::println);
        System.out.println("-----------------");
        System.out.println(articleRepo.findFirstByOrderByPrixDesc());
        System.out.println("-----------------");
        articleRepo.findTop5ByOrderByPrix().forEach(System.out::println);
        System.out.println("-----------------");
        Page<Article> page = articleRepo.findByPrixLessThan(250.0,
                PageRequest.of(1, 4, Sort.by("Description").and(Sort.by("prix").descending())));
        System.out.println(page.getTotalElements() + " " + (page.getNumber() + 1) + " / " + page.getTotalPages());
        page.getContent().forEach(System.out::println);

        System.out.println("---Pagination---");
        articleRepo.findByPrixGreaterThan(50.0, PageRequest.of(0, 4)).forEach(System.out::println);

        System.out.println("-----------------");
        articleRepo.findByPrixGreaterThan(50.0, Pageable.unpaged()).forEach(System.out::println);

        System.out.println("-----------------");
        articleRepo.findByPrixLessThanSQL(125.0).forEach(System.out::println);

        System.out.println("-----EntityGraph------------");
        Marque a = marqueRepo.findByNomLike("M%").get(0);
        a.getArticles().forEach(System.out::println);

        articleRepo.findByOrderByPrix().get(0).getFournisseurs().forEach(System.out::println);
        System.out.println(articleRepo.findByOrderByPrix().get(0).getMarque());

        System.out.println("-----Procédure Stockée------------");
        System.out.println(articleRepo.countInfMontant(20.0));
        System.out.println(articleRepo.get_count_by_prix2(20.0));
        System.out.println(articleRepo.countInfMontantSQL(20.0));

        System.out.println("-----auditing ------------");
        Marque m = marqueRepo.findById(3L).get();
        m.setNom("Marque C");
        marqueRepo.saveAndFlush(m);

        Marque d = new Marque();
        d.setNom("Marque D");
        marqueRepo.saveAndFlush(d);
        System.out.println("-----Repository Personnalisé ------------");
        employeRepo.findBy("John", null).forEach(System.out::println);
        employeRepo.findBy(null, "Dalton").forEach(System.out::println);
        employeRepo.findBy("Allan", "Smithee").forEach(System.out::println);
    
        System.out.println( pizzaRepo.findById(1L).get().getIngredients().get(0).getIngredient().getNom());
    }

}
