package fr.dawan.springboot.runners;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import fr.dawan.springboot.services.EmailService;

//@Component
public class EmailRunner implements ApplicationRunner {
    @Autowired
    private EmailService emailService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        emailService.sendSimpleMail("Un email de test", "test", "jd@dawan.com", "no-reply@dawan.com");
        
        emailService.sendHTMLMail("<html><body><h1>email HTML</h1></body></html>", "test", "jd@dawan.com", "no-reply@dawan.com");
        
        Map<String, Object> model = new HashMap<>();
        model.put("prenom", "John");
        model.put("email", "jdoe@dawan.com");
        emailService.sendTemplateMail("mailTemplate.ftlh", model, "un uml de test", "jd@dawan.com", "no-reply@dawan.com");

        emailService.sendMailAttachement("test mail", "test", "jd@dawan.com", "no-reply@dawan.com", new File("logo.jpg"));
    }

}
