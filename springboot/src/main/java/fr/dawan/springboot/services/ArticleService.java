package fr.dawan.springboot.services;

import fr.dawan.springboot.dto.ArticleDto;

public interface ArticleService extends GenericService<ArticleDto, Long> {

}
