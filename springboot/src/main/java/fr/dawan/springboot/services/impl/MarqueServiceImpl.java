package fr.dawan.springboot.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.entities.Marque;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.MarqueService;

@Service
@Transactional
public class MarqueServiceImpl implements MarqueService {

    @Autowired
    private MarqueRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<MarqueDto> getAllMarque(Pageable page) {
        return repository.findAll(page).stream().map(m -> mapper.map(m, MarqueDto.class)).collect(Collectors.toList());

    }

    @Override
    public MarqueDto getMarqueById(long id) {
        return mapper.map(repository.findById(id).get(), MarqueDto.class);
    }

    @Override
    public List<MarqueDto> getMarqueByNom(String nom) {
        return repository.findByNomLike("%"+nom+"%").stream().map(m -> mapper.map(m, MarqueDto.class)).collect(Collectors.toList());
    }

    @Override
    public boolean deleteMarque(long id) {
       // repository.deleteById(id);
        return repository.removeById(id)!=0;
    }

    @Override
    public MarqueDto saveOrUpdate(MarqueDto marqueDto) {
        Marque m = repository.saveAndFlush(mapper.map(marqueDto, Marque.class));
        return mapper.map(m, MarqueDto.class);
    }

}
