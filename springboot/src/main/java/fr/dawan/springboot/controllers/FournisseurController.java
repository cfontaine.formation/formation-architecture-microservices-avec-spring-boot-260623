package fr.dawan.springboot.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.dto.FournisseurDto;
import fr.dawan.springboot.services.FournisseurService;

@RestController
@RequestMapping("/api/v1/fournisseurs")
public class FournisseurController extends GenericController<FournisseurDto, Long> {

    public FournisseurController(FournisseurService service) {
        super(service);
    }
    
}
