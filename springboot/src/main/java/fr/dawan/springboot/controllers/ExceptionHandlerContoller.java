package fr.dawan.springboot.controllers;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.dawan.springboot.dto.ApiError;

@ControllerAdvice
public class ExceptionHandlerContoller extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SQLException.class)
    public ResponseEntity<?> handleConflict(SQLException e, WebRequest request) {
        ApiError err = new ApiError(HttpStatus.BAD_REQUEST, e.getMessage());
        return handleExceptionInternal(e, err, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatusCode status, WebRequest request) {

        Map<String, String> erreur = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(e -> {
            String attribut = ((FieldError) e).getField();
            String msg = e.getDefaultMessage();
            erreur.put(attribut, msg);
        });

        return new ResponseEntity(erreur, HttpStatus.BAD_REQUEST);
    }

}
