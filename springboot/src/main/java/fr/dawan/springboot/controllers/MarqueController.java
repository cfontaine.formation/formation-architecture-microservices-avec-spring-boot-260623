package fr.dawan.springboot.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.services.MarqueService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/v1/marques")
@Tag(name="Marques",description = "L'api des marques")
public class MarqueController {
    
    @Autowired
    private MarqueService service;
    
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
 //   @ResponseBody
    public List<MarqueDto> getAll(){
        return service.getAllMarque(Pageable.unpaged());
    }

    @GetMapping(params= {"size","page"},produces="application/json")
    public List<MarqueDto> getAll(Pageable page){
        return service.getAllMarque(page);
    }
    
    @Operation(summary = "Trouver les marques en fonction de leur id",description = "retourne des marques",tags="Marques")
    @ApiResponses({
        @ApiResponse(responseCode = "200", description="Opération réussit",content = @Content(schema = @Schema(implementation = MarqueDto.class))),
        @ApiResponse(responseCode = "404", description="Marque non trouvé")
    }
    )
    @GetMapping(value="/{id:[0-9]+}",produces =MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MarqueDto> getById(
            @Parameter(description = "L'id de la marque",required = true,allowEmptyValue = false)
            @PathVariable long id) {
        try {
           // return ResponseEntity.ok().header("test", "test").body(service.getMarqueById(id));)
            return ResponseEntity.ok(service.getMarqueById(id));
        } catch (Exception e) {
           return ResponseEntity.notFound().build(); // 404
        }
    }
    
    @GetMapping(value="/{nom:[a-zA-Z][a-zA-Z0-9]+}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MarqueDto> getByNom(@PathVariable String nom) {
        return service.getMarqueByNom(nom);
    }
    
    @ResponseStatus(value=HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto create(@RequestBody MarqueDto marqueDto) {
        return service.saveOrUpdate(marqueDto);
        }
    
    @DeleteMapping(value="/{id}",produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> delete(@PathVariable long id) {
       if( service.deleteMarque(id)) {
           return new ResponseEntity<>("La marque "+ id + " est supprimée",HttpStatus.OK);
       }
       else {
           return new ResponseEntity<>("La marque "+ id + " n'existe pas",HttpStatus.NOT_FOUND);
       }
    }
    
    @PutMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto update(@PathVariable  long id, @RequestBody MarqueDto marqueDto) {
        MarqueDto md=service.getMarqueById(id);
        if(md!=null) {
            md.setNom(marqueDto.getNom());
        }
        return service.saveOrUpdate(md);
    }
    
    // Exception
    @GetMapping("/ioexception")
    public void genIOException() throws IOException{
        throw new IOException("Erreur entrée / sortie");
    }
    
    @GetMapping("/sqlexception")
    public void genSQLException() throws SQLException{
        throw new SQLException("Erreur SQL");
    }
    
    
    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> handlerException(IOException e) {
        return ResponseEntity.badRequest().body(e.getMessage());
    }
    
}
