package fr.dawan.springboot.controllers;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.springboot.dto.MarqueDto;

@RestController
public class ClientMarqueController {
    @Autowired
    private RestTemplate restTemplate;

    // Pour ajouter dans l'entête de la requête autorization -> HTTP basic
    private HttpHeaders createHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", encodePasswordBase64("admin", "123456"));
        return headers;
    }
    
    private String encodePasswordBase64(String username, String password) {
        String auth = username + ":" + password;
        String authBase64 = new String(Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII"))));
        return "Basic " + authBase64;
    }

    @GetMapping(value="/client/marques",produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAll() {
//      // Sans authentification
//       MarqueDto [] marquesDto= restTemplate.getForObject("http://localhost:8080/api/v1/marques", MarqueDto[].class);
//       return new ResponseEntity<>(marquesDto),HttpStatus.OK);

        // Avec une authentification Http Basic
        HttpEntity<String> entity = new HttpEntity<String>(createHeaders());
        // la méthode exchange permet de modifier le l'en-tête et le corps de la requête
        return restTemplate.exchange("http://localhost:8080/api/v1/marques", HttpMethod.GET, entity, MarqueDto[].class);
    }

    @GetMapping(value="/client/marques/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getById(@PathVariable long id) {
//      // Sans authentification
//      MarqueDto marqueDto= restTemplate.getForObject("http://localhost:8080/api/v1/marques",
//      MarqueDto.class,Map.of("id",id));
//      return new ResponseEntity<>(marqueDto),HttpStatus.OK);

        // Avec une authentification Http Basic
        HttpEntity<String> entity = new HttpEntity<String>(createHeaders());
        return restTemplate.exchange("http://localhost:8080/api/v1/marques/{id}", HttpMethod.GET, entity,
                MarqueDto.class,Map.of("id",id));
    }

    @PostMapping(value="/client/marques",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MarqueDto> create (@RequestBody MarqueDto dto) throws StreamWriteException, DatabindException, IOException{
        ObjectMapper objectMapper=new ObjectMapper();
        HttpEntity<String> entity = new HttpEntity<String>(objectMapper.writeValueAsString(dto),createHeaders());
        return restTemplate.exchange("http://localhost:8080/api/v1/marques", HttpMethod.POST, entity,MarqueDto.class);
    }
    
    @DeleteMapping(value="/client/marques/{id}",produces=MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> delete(@PathVariable long id){
        HttpHeaders h=new HttpHeaders();
        h.set("Authorization", encodePasswordBase64("admin", "123456"));
        HttpEntity<String> entity = new HttpEntity<String>(h);
        return restTemplate.exchange("http://localhost:8080/api/v1/marques/{id}",HttpMethod.DELETE,entity,String.class,Map.of("id",id));
    }
    
    @PutMapping(value="/client/marques/{id}",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MarqueDto> update (@PathVariable long id,@RequestBody MarqueDto dto) throws Exception {
        ObjectMapper objectMapper=new ObjectMapper();
        HttpEntity<String> entity = new HttpEntity<String>(objectMapper.writeValueAsString(dto),createHeaders());
        return restTemplate.exchange("http://localhost:8080/api/v1/marques/{id}", HttpMethod.PUT, entity,MarqueDto.class,Map.of("id",id));
    }
 
}
