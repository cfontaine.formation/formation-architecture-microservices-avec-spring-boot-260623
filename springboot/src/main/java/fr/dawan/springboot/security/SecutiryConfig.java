package fr.dawan.springboot.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecutiryConfig {

    @Bean
    PasswordEncoder encoder() {
        //return PasswordEncoderFactories.createDelegatingPasswordEncoder();
        return new BCryptPasswordEncoder();
    }
    
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf(c->c.disable())
        .authorizeHttpRequests(auth -> auth
                .requestMatchers("/client/marques/**").permitAll()
                .requestMatchers(HttpMethod.POST).hasAnyAuthority("WRITE","ADMIN")
                .requestMatchers(HttpMethod.DELETE).hasAnyAuthority("DELETE","ADMIN")
                .requestMatchers(HttpMethod.PUT).hasAnyAuthority("WRITE","ADMIN")
                .requestMatchers(HttpMethod.GET).hasAnyAuthority("READ","ADMIN")
                .anyRequest().permitAll()).httpBasic(Customizer.withDefaults());
        return http.build();
    }
}
