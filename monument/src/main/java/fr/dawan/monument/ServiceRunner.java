package fr.dawan.monument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import fr.dawan.monument.dto.EtiquetteDto;
import fr.dawan.monument.services.EtiquetteService;
@Component
public class ServiceRunner implements ApplicationRunner {

    @Autowired
    private EtiquetteService service;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        service.getAllEtiquette(PageRequest.of(0, 4)).forEach(System.out::println);
        
        System.out.println(service.getEtiquetteById(2L));
        
        EtiquetteDto e=new EtiquetteDto(0L,"Monument historique");

        e=service.saveOrUpdate(e);
        System.out.println(e);
        service.deleteEtiquette(e.getId());
    }

}
