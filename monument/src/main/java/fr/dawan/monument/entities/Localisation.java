package fr.dawan.monument.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString

@Entity
@Table(name = "localisations")
public class Localisation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Include
    private long id;

    @Version
    private int version;

    @Column(nullable = false, length = 80)
    private String ville;
    
    @Column(nullable = false, length = 80)
    private String pays;
    
    @OneToMany(mappedBy="localisation")
    @Exclude
    private List<Monument> monuments=new ArrayList<>();

    public Localisation(String ville, String pays) {
        this.ville = ville;
        this.pays = pays;
    }
}
