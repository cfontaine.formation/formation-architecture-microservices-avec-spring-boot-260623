package fr.dawan.monument.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString

@Entity
@Table(name = "monuments")
public class Monument implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Include
    private long id;

    @Version
    private int version;

    @Column(nullable = false, length = 150)
    private String nom;

    @Column(nullable = false)
    private int anneeConstruction;

    private String description;

    @Lob
    @Column(length = 65000)
    @Exclude
    private byte[] photo;

    @OneToOne
    @Exclude
    private Coordonne coordone;

    @ManyToOne
    @Exclude
    private Localisation localisation;

    @ManyToMany
    @Exclude
    private List<Etiquette> etiquettes = new ArrayList<>();

    public Monument(String nom, int anneeConstruction, String description) {
        this.nom = nom;
        this.anneeConstruction = anneeConstruction;
        this.description = description;
    }
}
