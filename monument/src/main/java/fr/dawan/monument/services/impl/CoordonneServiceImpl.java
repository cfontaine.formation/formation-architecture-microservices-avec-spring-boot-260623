package fr.dawan.monument.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.monument.dto.CoordonneDto;
import fr.dawan.monument.entities.Coordonne;
import fr.dawan.monument.repositories.CoordonneRepository;
import fr.dawan.monument.services.CoordonneService;
@Service
@Transactional
public class CoordonneServiceImpl extends GenericServiceImpl<CoordonneDto, Coordonne, Long>
        implements CoordonneService {

    public CoordonneServiceImpl(CoordonneRepository repository, ModelMapper mapper) {
        super(repository, mapper, Coordonne.class, CoordonneDto.class);
    }

}
