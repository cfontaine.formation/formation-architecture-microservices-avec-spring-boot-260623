package fr.dawan.monument.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.monument.dto.EtiquetteDto;
import fr.dawan.monument.entities.Etiquette;
import fr.dawan.monument.repositories.EtiquetteRepository;
import fr.dawan.monument.services.EtiquetteService;

@Service
@Transactional
public class EtiquetteServiceImpl implements EtiquetteService {
    @Autowired
    private EtiquetteRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<EtiquetteDto> getAllEtiquette(Pageable page) {
        return repository.findAll(page).stream().map(e -> mapper.map(e, EtiquetteDto.class)).collect(Collectors.toList());

    }

    @Override
    public EtiquetteDto getEtiquetteById(long id) {
        return mapper.map(repository.findById(id).get(), EtiquetteDto.class);
    }

   
    @Override
    public boolean deleteEtiquette(long id) {
        return repository.removeById(id)!=0;
    }

    @Override
    public EtiquetteDto saveOrUpdate(EtiquetteDto etiquetteDto) {
        return mapper.map(repository.saveAndFlush(mapper.map(etiquetteDto, Etiquette.class)), EtiquetteDto.class);
    }
}
