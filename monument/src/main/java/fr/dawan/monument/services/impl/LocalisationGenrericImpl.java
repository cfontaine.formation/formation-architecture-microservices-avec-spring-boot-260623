package fr.dawan.monument.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.monument.dto.LocalisationDto;
import fr.dawan.monument.entities.Localisation;
import fr.dawan.monument.repositories.LocalisationRepository;
import fr.dawan.monument.services.LocalisationService;

@Service
@Transactional
public class LocalisationGenrericImpl extends GenericServiceImpl<LocalisationDto, Localisation, Long>
        implements LocalisationService {

    public LocalisationGenrericImpl(LocalisationRepository repository, ModelMapper mapper) {
        super(repository, mapper, Localisation.class, LocalisationDto.class);

    }

  

}
