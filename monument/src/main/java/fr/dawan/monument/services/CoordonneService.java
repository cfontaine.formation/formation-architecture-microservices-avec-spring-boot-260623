package fr.dawan.monument.services;

import fr.dawan.monument.dto.CoordonneDto;

public interface CoordonneService extends GenericService<CoordonneDto, Long> {

}
