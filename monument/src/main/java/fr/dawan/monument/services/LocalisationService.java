package fr.dawan.monument.services;

import fr.dawan.monument.dto.LocalisationDto;

public interface LocalisationService extends GenericService<LocalisationDto, Long> {

}
