package fr.dawan.monument.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.monument.services.GenericService;

public abstract class GenericServiceImpl<TDto,T,ID> implements GenericService<TDto, ID> {
    
    protected JpaRepository<T, ID> repository;
    
    protected ModelMapper mapper;
    
    private Class<T> clazz;
    
    private Class<TDto> clazzDto;
    
    
    public GenericServiceImpl(JpaRepository<T, ID> repository, ModelMapper mapper, Class<T> clazz,
            Class<TDto> clazzDto) {
        this.repository = repository;
        this.mapper = mapper;
        this.clazz = clazz;
        this.clazzDto = clazzDto;
    }

    @Override
    public Page<TDto> getAll(Pageable page) {
        Page<T> p=repository.findAll(page);
        List<TDto> lst=p.getContent().stream().map(e -> mapper.map(e, clazzDto) ).collect(Collectors.toList());
        return new PageImpl<>(lst, p.getPageable(), p.getTotalElements()); 
    }

    @Override
    public TDto getById(ID id) {
       return mapper.map(repository.findById(id),clazzDto);
    }

    @Override
    public boolean delete(ID id) {
        repository.deleteById(id);
        return true;
    }

    @Override
    public TDto saveOrUpdate(TDto dto) {
        return mapper.map(repository.saveAndFlush(mapper.map(dto, clazz)),clazzDto);

    }

}
