package fr.dawan.monument.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.monument.dto.EtiquetteDto;

public interface EtiquetteService {

    List<EtiquetteDto> getAllEtiquette(Pageable page);

    EtiquetteDto saveOrUpdate(EtiquetteDto etiquetteDto);

    EtiquetteDto getEtiquetteById(long id);

    boolean deleteEtiquette(long id);
}
