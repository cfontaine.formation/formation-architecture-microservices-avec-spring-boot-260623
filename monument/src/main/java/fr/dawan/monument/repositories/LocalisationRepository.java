package fr.dawan.monument.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.monument.entities.Localisation;

public interface LocalisationRepository extends JpaRepository<Localisation, Long> {

}
