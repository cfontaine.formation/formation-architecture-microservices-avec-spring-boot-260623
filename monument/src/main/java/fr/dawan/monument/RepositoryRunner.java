package fr.dawan.monument;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.monument.repositories.MonumentRepository;
//@Component
public class RepositoryRunner implements ApplicationRunner {

    @Autowired
    private MonumentRepository monumentRepository;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
      monumentRepository.findByAnneeConstructionBetweenOrderByAnneeConstructionDesc(2000, 2022).forEach(m-> System.out.println(m));  
      System.out.println("_________________________________________");
      monumentRepository.findByNomLike("T__%").forEach(m-> System.out.println(m));
      System.out.println("_________________________________________");
      System.out.println(monumentRepository.findByCoordoneLatitudeAndCoordoneLongitude(41.8905556,12.4925));
      System.out.println("_________________________________________");
      monumentRepository.findByLocalisationPaysIn(Arrays.asList("France","Italie"),Pageable.unpaged()).forEach(m-> System.out.println(m));
      System.out.println("_________________________________________");
      monumentRepository.findTop5ByOrderByAnneeConstruction().forEach(m-> System.out.println(m));
      System.out.println("_________________________________________");
      monumentRepository.findByEtiquetteIntitule("Statue").forEach(m-> System.out.println(m));
    }

}
